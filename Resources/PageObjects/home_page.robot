***Settings***
Library     SeleniumLibrary

***Variables***
${your_feed_link}       xpath: //a[normalize-space()='Your Feed']
${global_feed_link}     xpath: //a[normalize-space()='Global Feed']

***Keywords***
Verify Your Feed Link is Visible
    Wait Until Element Is Visible   ${your_feed_link} 
    Element Should Be Visible       ${your_feed_link} 

Verify Your Global Feed is Visible
    Wait Until Element Is Visible   ${global_feed_link}
    Element Should Be Visible       ${global_feed_link}