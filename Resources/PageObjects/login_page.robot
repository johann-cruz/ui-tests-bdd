***Settings***
Library     SeleniumLibrary
Resource    ../../TestData/config_data.robot

***Variables***
# Locators
${signin_link}      xpath: //a[normalize-space()='Sign in']
${email_field}      xpath: //input[@placeholder='Email']
${password_field}   xpath: //input[@placeholder='Password']
${signin_button}    xpath: //button[normalize-space()='Sign in']
${error_label}      xpath: //app-root/app-auth-page/div[@class='auth-page']//app-list-errors/ul[@class='error-messages']/li[.=' email or password is invalid ']


***Keywords***
Go To Login Page
    Wait Until Element Is Visible   ${signin_link}
    Element Should Be Visible       ${signin_link}
    Click Element                   ${signin_link}

Login Page Opened
    Wait Until Element Is Visible   ${email_field}
    Element Should Be Visible       ${email_field}
    Wait Until Element Is Visible   ${password_field} 
    Element Should Be Visible       ${password_field}

Input Email
    [Arguments]                     ${email}
    Wait Until Element Is Enabled   ${email_field}
    Input Text                      ${email_field}          ${email}

Input Password
    [Arguments]                     ${password}
    Wait Until Element Is Enabled   ${password_field}
    Input Text                      ${password_field}       ${password}

Sign-in Button Should Be Enabled
    Wait Until Element Is Enabled   ${signin_button}
    Element Should Be Enabled       ${signin_button}  

Click Sign-in Button
    Wait Until Element Is Enabled     ${signin_button}
    Click Element                     ${signin_button}

Error Message Displayed
    [Arguments]     ${error}
    Wait Until Element Contains     ${error_label}    ${error}