***Settings***
Resource    ../PageObjects/login_page.robot

***Keywords***
User opens login page
    login_page.Go To Login Page
    login_page.Login Page Opened

User inputs credentials
    [Arguments]     ${email}    ${password}
    login_page.Input Email  ${email}
    login_page.Input Password  ${password}
    login_page.Click Sign-in Button

User is unable to login
    [Arguments]     ${error_msg}
    login_page.Error Message Displayed  ${error_msg}