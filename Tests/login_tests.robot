***Settings***
Library             SeleniumLibrary
Resource            ../Resources/Commons/resources.robot
Resource            ../TestData/config_data.robot
Resource            ../Resources/Steps/login_steps.robot
Resource            ../Resources/Steps/home_steps.robot

Suite Setup         resources.Start TestCase
Suite Teardown      resources.Finish TestCase

***Variables***
${error_message}    email or password is invalid


***Test Cases***
Invalid Login Scenarios
    [Documentation]     User will be unable to login using invalid credentials
    GIVEN User opens login page 
    WHEN User inputs credentials     ${email_invalid}       ${password_invalid}
    THEN User is unable to login     ${error_message}

Valid Login
    [Documentation]     User will be able to login using valid credentials
    GIVEN User opens login page 
    WHEN User inputs credentials     ${email_valid}       ${password_valid}
    THEN User will be able to access Home Page